<nav>
  <div class="nav-wrapper">
    <ul class="right hide-on-med-and-down">
      <!-- Authentication Links -->
      @guest
        <li><a href="{{ route('login') }}">Login</a></li>
        <li><a href="{{ route('register') }}">Register</a></li>
      @else
        <!-- Dropdown Trigger -->
        <li>
          <a class="dropdown-button" href="#!" data-activates="login-dropdown">
            {{ Auth::user()->name }}<i class="material-icons right">arrow_drop_down</i>
          </a>
          <!-- Dropdown Structure -->
          <ul id="login-dropdown" class="dropdown-content">
            <li>
              <a href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                  Logout
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
              </form>
            </li>
          </ul>
        </li>
      @endguest
    </ul>
  </div>
</nav>
