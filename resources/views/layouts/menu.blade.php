<div class="container">
  <nav class="menu">
    <div class="nav-wrapper">
      <ul class="left hide-on-med-and-down">
        <li><a href="{{ route('home') }}">HOME</a></li>
        @if(Auth::check())
          <li><a href="{{ route('product.index') }}">PRODUTOS</a></li>
        @endif
      </ul>
    </div>
  </nav>
</div>
