<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Test LetsGrow!</title>

    <!-- Styles -->
    {!! MaterializeCSS::include_css() !!}
    <link rel="stylesheet" href="/css/application.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
</head>
<body>
    @include('layouts.navbar')

    @include('layouts.menu')

    <div class="container">
      @yield('content')
    </div>

    <!-- Scripts -->
    <script type="text/javascript" src="{{ asset('/js/jquery-3.2.1.min.js') }}"></script>
    {!! MaterializeCSS::include_js() !!}
    @yield('scripts')
</body>
</html>
