@if ($errors->any())
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif


Título: {!! Form::text('title', $product->title ?? '') !!}<br>
Descrição: {!! Form::text('description', $product->description ?? '') !!}<br>
Image: {!! Form::file('image', '') !!}<br>
{{-- Thumbnail: {!! Form::file('thumbnail', '') !!}<br> --}}
Preço: {!! Form::text('price', $product->price ?? '') !!}<br>
{!! Form::submit('Enviar', ['class' => 'waves-effect waves-light btn']) !!}
