@extends('layouts.app')
@section('content')
  <h1>Detalhes do Produto</h1>
  Titulo :{{ $product->title }}<br>
  Descrição: {{ $product->description }}<br>
  Imagem: <img src="{{ $product->image}}" alt=""><br>
  Thumbnail: <img src="{{ $product->thumbnail }}" alt=""><br>
  Preço: {{ $product->price }}<br>
@endsection
