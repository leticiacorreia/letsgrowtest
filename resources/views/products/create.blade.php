@extends('layouts.app')
@section('content')
  <h1>Criação de Produto</h1>
  {!! Form::open([
    'route' => 'product.store',
    'method' => 'POST',
    'files' => true,
    ]) !!}

  @include('products.form')

  {!! Form::close() !!}
@endsection
