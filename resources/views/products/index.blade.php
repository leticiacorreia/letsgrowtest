@extends('layouts.app')
@section('content')
  <a href="{{ route('product.create') }}" class="waves-effect waves-light btn">Criar Novo Produto</a>
  <table>
    <thead>
      <tr>
        <th>Título</th>
        <th>Descrição</th>
        <th>Thumbnail</th>
        <th>Preço</th>
        <th>Ações</th>
      </tr>
    </thead>
    <body>
      @forelse  ($products as $product)
        <tr>
          <td>{{ $product->title }}</td>
          <td>{{ $product->description }}</td>
          <td><img src="{{ $product->thumbnail }}" alt=""></td>
          <td>{{ $product->price }}</td>
          <td>
            <a href="{{ route('product.show', $product->id) }}" class="waves-effect waves-light btn">Ver</a>
            <a href="{{ route('product.edit', $product->id) }}" class="btn waves-effect waves-teal">Editar</a>
            <span onclick="javascript:requestDelete(this)" data-route="{{ route('product.destroy', $product->id) }}" class="btn waves-effect waves-light red">Excluir</span>
          </td>
        </tr>
      @empty
        Nenhum produto registrado.
      @endforelse
    </body>
  </table>
@endsection

@section('scripts')
  <script>
    function requestDelete(element){
      let route = element.getAttribute('data-route');
      let token = $('meta[name="csrf-token"]').attr('content');

      $.ajax({
        url: route,
        type: 'post',
        data: {_token: token, _method: 'delete'},
        success: function(data, status){
          console.log(data);
          console.log(status);
        }
      });
    }
  </script>
@endsection
