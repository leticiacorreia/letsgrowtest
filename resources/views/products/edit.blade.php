@extends('layouts.app')
@section('content')
  <h1>Edição de Produto</h1>
  {!! Form::open([
    'route' => ['product.update', $product->id],
    'method' => 'put',
    ]) !!}

  @include('products.form')
  {!! Form::close() !!}
@endsection
