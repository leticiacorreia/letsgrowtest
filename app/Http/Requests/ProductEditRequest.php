<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
         return [
             'title' => 'required',
             'description' => 'required',
             'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
             'price' => 'required',
         ];
     }

     public function messages()
     {
       return [
           'title.required' => 'O campo título deve ser preenchido.',
           'description.required' => 'O campo descrição deve ser preenchido.',
           'image.required' => 'O campo imagem deve conter um arquivo.',
           'image.mimes'  => 'A imagem deve ser do tipo (png, jpg, jpeg)',
           'price.required' => 'O campo preço deve ser preenchido.',
         ];
     }
}
