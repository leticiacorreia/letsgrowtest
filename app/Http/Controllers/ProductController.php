<?php

namespace App\Http\Controllers;

// require  'vendor\autoload.php';

use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\ProductEditRequest;
use Intervention\Image\ImageManagerStatic as Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $product = $request->all();

        if ($request->hasFile('image')) {

            Image::configure(array('driver' => 'imagick'));

            $image_file = $request->file('image');
            $image = Image::make($image_file);
            $image->resize(400, 350);

            $extension = $image_file->getClientOriginalExtension();
            $image_path = '/' . 'images/' . time() . '.' . $extension;
            $image->save(public_path($image_path));

            $thumb = $image->fit(200, 150);
            $thumb_path = '/images/thumbs/' . time() . '-thumb.' . $extension;
            $thumb->save(public_path($thumb_path));

            $product['image'] = $image_path;
            $product['thumbnail'] = $thumb_path;

        }

        Product::create($product);

        return redirect(route('product.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        return view('products.show', compact(['product']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductEditRequest $request, $id)
    {
        $data = $request->all();
        $product = Product::find($id);
        $product->fill($data);
        $product->save();
        return redirect(route('product.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
          $product = Product::find($id);
          $img = $product->image;
          $thumb = $product->thumbnail;
          unlink(public_path($img));
          unlink(public_path($thumb));

          Product::destroy($id);
          return response(['msg' => 'Product Deleted', 'status' => 'success']);
        }
        return response(['msg' => 'Failed delete the product.', 'status' => 'Failed']);
    }

}
